const usercontroller = require("../controllers").user;

module.exports = app => {
  app.get("/api", (req, res) => {
    res.json({
      status: "success",
      message: "MOE API",
      data: { version_number: "v1.0.0" }
    });
  });

  app.post("/api/login", usercontroller.login);
};
